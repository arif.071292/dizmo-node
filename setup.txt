// Ubuntu 14.04 and later versions

// Node Js

curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -

sudo apt-get install -y nodejs

sudo apt-get install -y build-essential


// MongoDB

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10

echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list

sudo apt-get update

sudo apt-get install -y mongodb-org


// Elastic Search

 - install oracle java

	sudo add-apt-repository ppa:webupd8team/java
	sudo apt-get update
	sudo apt-get install oracle-java8-installer

 - Or use openjdk

	sudo apt-get update
	sudo apt-get install openjdk-7-jre

 - check java 

 	java -version

 - installation

 	wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
 	echo "deb http://packages.elastic.co/elasticsearch/2.x/debian stable main" | sudo tee -a /etc/apt/sources.list.d/elasticsearch-2.x.list
 	sudo apt-get update && sudo apt-get install elasticsearch

- add to system startup
	sudo update-rc.d elasticsearch defaults 95 10
	

// Run

 - start mongodb
 	mongod --dbpath ./data-location

 - mongodb shell
 	mongo

 - start elasticsearch
 	sudo /etc/init.d/elasticsearch start

 - finally
 	cd location
 	npm install
 	nodemon
