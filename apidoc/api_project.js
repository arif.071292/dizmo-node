define({
  "name": "Dizmo",
  "version": "0.0.2",
  "description": "Dizmo RESTful API",
  "title": "Dizmo API",
  "url": "localhost:3000",
  "order": [
    "GetUser",
    "PostUser"
  ],
  "template": {
    "withCompare": true,
    "withGenerator": true
  },
  "sampleUrl": false,
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2016-02-25T08:21:44.580Z",
    "url": "http://apidocjs.com",
    "version": "0.15.1"
  }
});
