var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// SCHEMA
var CategorySchema = new Schema({
	name 	: {type : String, trim: true, index : true, required : true, unique : true},
	created : Date,
	updated : Date
});


module.exports 	= mongoose.model('Category',CategorySchema);