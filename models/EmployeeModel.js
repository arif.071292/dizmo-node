var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// SCHEMA
var EmployeeSchema = new Schema({
	userID 		: {type : String, trim: true, index : true, required : true, unique : true},
	email 		: {type : String, trim: true, index : true, required : true, unique : true},
	firstname 	: {type : String, trim: true},
	lastname 	: {type : String, trim: true},
	website 	: {type : String, trim: true},
	description : {type : String, trim: true},
	coverphoto 	: {type : String, trim: true},
	propic 		: {type : String, trim: true},
	country 	: {type : String, trim: true},
	gender 		: {type : String, trim: true},
	created 	: Date,
	updated 	: Date
});


module.exports 	= mongoose.model('Employee',EmployeeSchema);