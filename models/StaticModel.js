var mongoose = require('mongoose');
var i18nPlugin = require ('mongoose-i18n');
var Schema = mongoose.Schema;


// SCHEMA
var StaticSchema = new Schema({
	attribute	: {type : String, trim : true, index : true, required : true, unique : true, i18n : true},
	description	: {type : String, i18n : true},
	created 	: Date,
	updated 	: Date
});


// il8n Support
StaticSchema.plugin(i18nPlugin, {languages: ['en', 'bn'], defaultLanguage: 'en'});


module.exports 	= mongoose.model('Static',StaticSchema);