var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// SCHEMA
var UserSchema = new Schema({
	email 		: {type : String, trim : true, required : true, unique : true, index : true},
	password 	: {type : String, trim : true, required : true},
	role 		: {type : String, trim : true, required : true},
	secretID 	: {type : String, trim : true, required : true},
	verified	: {type : Boolean, default : false},
	active		: {type : Boolean, default : true},
	created 	: Date,
	updated 	: Date
});


module.exports 	= mongoose.model('User', UserSchema);