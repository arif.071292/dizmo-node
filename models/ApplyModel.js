var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// SCHEMA
var ApplySchema = new Schema({
	jobID 		: {type : String, trim: true, required : true, index : true},
	employeeID	: {type : String, trim: true, required : true, index : true},
	created 	: Date
});


// Compound Indexes
ApplySchema.index({jobID : 1, employeeID : 1}, {unique : true}); 


module.exports 	= mongoose.model('Apply',ApplySchema);