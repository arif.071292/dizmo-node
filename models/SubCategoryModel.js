var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// SCHEMA
var SubCategorySchema = new Schema({
	name 		: {type : String, trim: true, index : true, required : true},
	categoryID 	: {type : String, trim: true, index : true, required : true},
	created 	: Date,
	updated 	: Date
});


// Compound Indexes
SubCategorySchema.index({ name: 1, categoryID: 1 }, {unique : true});


module.exports 	= mongoose.model('SubCategory',SubCategorySchema);