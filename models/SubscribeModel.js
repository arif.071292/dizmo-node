var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// SCHEMA
var SubscribeSchema = new Schema({
	employeeID 	: {type : String, trim: true, index : true, required : true},
	categoryID 	: {type : String, trim: true, index : true, required : true},
	created 	: Date
});


// Compound Indexes
SubscribeSchema.index({ employeeID: 1, categoryID: 1 }, {unique : true});


module.exports 	= mongoose.model('Subscribe',SubscribeSchema);