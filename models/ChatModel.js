var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// SCHEMA
var ChatSchema = new Schema({
	employerID	: {type : String, trim: true, required : true, index : true},
	employeeID	: {type : String, trim: true, required : true},
	message		: {type : String, trim: true, required : true},
	created 	: Date
});


module.exports 	= mongoose.model('Chat',ChatSchema);