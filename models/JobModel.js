var mongoose = require('mongoose');
var mongoosastic = require('mongoosastic');
var Schema = mongoose.Schema;


// SCHEMA
var JobSchema = new Schema({
	title 		: {type : String, trim: true, index : true, required : true, es_indexed:true, es_boost : 10.0},
	category	: {type : String, trim: true, index : true, required : true, es_indexed:true, es_boost : 6.0},
	subcategory	: {type : String, trim: true, index : true, required : true, es_indexed:true, es_boost : 5.0},
	employerName: {type : String, trim: true, index : true, required : true, es_indexed:true, es_boost : 7.0},
	employerMail: {type : String, trim: true, index : true, required : true},
	description	: {type : String, es_indexed:true, es_boost : 1.0},
	featured	: {type : Boolean, default: false},
	active 		: {type : Boolean, default: true},
	deadline	: Date,
	created 	: Date,
	updated 	: Date
});


// Compound Indexes
JobSchema.index({ title: 1, category: 1, employerName: 1}, {unique : true});


// Elastic Search
JobSchema.plugin(mongoosastic)


module.exports 	= mongoose.model('Job',JobSchema);