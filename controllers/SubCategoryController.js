var express 	= require('express')
var router 		= express.Router()
var Category 	= require('../models/CategoryModel')
var SubCategory = require('../models/SubCategoryModel')
var auth 		= require('../middlewares/auth')


// Middlewares
router.use(function(req, res, next){
    if (auth.isAuthenticated(req, res, next)) {
        next();
    }
});


// ---------------------------------------------------------
// ******************** SUB-CATEGORY ***********************
// ---------------------------------------------------------

var isValidCategory = function(ID){
	return true
}

/**
 * @api {post} /subcategory Create New SubCategory
 * @apiName PostSubCategory
 * @apiGroup SubCategory
 * @apiPermission Admin
 *
 * @apiParam {String} 	name		Mandatory Sub-Category Name.
 * @apiParam {String} 	categoryID	Mandatory Category ID.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.post('/', function(req, res, next) {
	if (req.body.name)
		name = req.body.name

	if (req.body.categoryID)
		categoryID = req.body.categoryID

    if (!req.body.name || name.length < 1 || !req.body.categoryID || categoryID.length != 24){
		res.status(400).json({
			success : false,
			message : "CategoryID and Sub-Category name can't be null."
		})
	} else{
	    Category.findOne({ _id:categoryID }, function(err, cat){
	    	if (err){
	    		res.status(500).json({
	    			success : false,
	    			message : "Internal Server Error!"
	    		})
	    	} else {
		    	if (!cat){
		    		res.status(404).json({
		    			success : false,
		    			message : "Provides a Valid Category."
		    		})
		    	} else{
		    		subcategory = new SubCategory()
		    		subcategory.name = name
		    		subcategory.categoryID = categoryID
				    subcategory.created = new Date()
				    subcategory.updated = new Date()

				    subcategory.save(function(error, data){
				        if (!error){
				            res.json({
								success: true,
								message: "New Sub Category Added."
							})
				        } else {
				            res.status(500).json({
								success: false,
								message: "Internal Server Error! Combination of Category ID and Sub-Category name must be unique!"
							})
				        }
				    })
		    	}
		    }
	    })
	}
})


/**
 * @api {get} /subcategory Request All SubCategory List
 * @apiName GetSubCategoryList
 * @apiGroup SubCategory
 * @apiPermission None
 *
 * @apiSuccess {String} _id				SubCategory unique ID.
 * @apiSuccess {String} name 			Name of the SubCategory.
 * @apiSuccess {String} categoryID  	Category ID.
 */

router.get('/', function(req, res) {
	var query = SubCategory.find({}).sort({categoryID:1,name:1}).select('name categoryID')

	query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "Sub-Category List",
				data: data
			})
	    } else {
	        res.status(400).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api {get} /subcategory/:id Request a Specific SubCategory Info
 * @apiName GetSubCategory
 * @apiGroup SubCategory
 * @apiPermission None
 *
 * @apiParam {String} id SubCategory Unique ID.
 *
 * @apiSuccess {String} _id				SubCategory unique ID.
 * @apiSuccess {String} name 			Name of the SubCategory.
 * @apiSuccess {String} categoryID  	Category ID.
 */
 
router.get('/:id', function(req, res) {
	var ID = req.params.id
	var query = SubCategory.findOne({ _id:ID }).select('name categoryID')

	query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "Sub-Category Name of Given ID.",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api {get} /subcategory/category/:id Request All SubCategory of Specific subcategory
 * @apiName GetSpecificSubCategoryList
 * @apiGroup SubCategory
 * @apiPermission None
 *
 * @apiParam {String} id Category Unique ID.
 *
 * @apiSuccess {String} _id				SubCategory unique ID.
 * @apiSuccess {String} name 			Name of the SubCategory.
 * @apiSuccess {String} categoryID  	Category ID.
 */

router.get('/category/:id', function(req, res) {
	var ID = req.params.id
	var query = SubCategory.find({ categoryID:ID }).sort({name:1}).select('name categoryID')

	query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "Sub-Category List of Given Category.",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api {put} /subcategory/:id Update Specific SubCategory
 * @apiName UpdateSubCategory
 * @apiGroup SubCategory
 * @apiPermission Admin
 *
 * @apiParam {String} id SubCategory's unique ID.
 *
 * @apiParam {String} 	[name]			Sub-Category Name.
 * @apiParam {String} 	[categoryID]	Category ID.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.put('/:id', function(req, res) {
	var ID = req.params.id

    if (!ID || ID.length != 24){
		res.status(400).json({
			success : false,
			message : "ID Required and It's length must be 24."
		})
	} else{
	    SubCategory.findOne({ _id:ID }, function(err, subcategory){
	    	if (err) {
	    		res.status(500).json({
					success: false,
					message: "Internal Server Error."
				})
	    	}

	        if (!subcategory) {
	            res.status(404).json({
					success: false,
					message: "Doesn't Exists!"
				})
	        } else {
	        	if (req.body.name && req.body.name.length > 0)
	        		subcategory.name = req.body.name

	        	if (req.body.categoryID && req.body.categoryID.length == 24)
	        		subcategory.categoryID = req.body.categoryID

	        	subcategory.update = new Date()
	        	subcategory.save(function(error){
			        if (!error){
			            res.json({
							success: true,
							message: "Successfully Updated."
						})
			        } else {
			            res.status(500).json({
							success: false,
							message: "Internal Server Error, Update Failed!"
						})
			        }
			    })
	        }
	    })
	}
})


/**
 * @api {delete} /subcategory/:id Delete Specific SubCategory
 * @apiName DeleteSubCategory
 * @apiGroup SubCategory
 * @apiPermission Admin
 *
 * @apiParam {String} id Employers unique ID.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.delete('/:id', function(req, res) {
	var ID = req.params.id
    
    SubCategory.findOne({ _id:ID }, function(err, data){
		if (err){
			res.status(500).json({
				success : false,
				message : "Internal Server Error"
			});
		} else{
			if (data){
				data.remove({ _id:ID },function(error){
					if (error){
						res.status(500).json({
							success : false,
							message : "Internal Server Error"
						})
					} else{
						res.json({
							success : true,
							message : "Successfully Deleted!"
						})
					}
				})
			} else{
				res.status(404).json({
					success : false,
					message : "Doesn't Exists!"
				})
			}
		}
	})
})


module.exports = router