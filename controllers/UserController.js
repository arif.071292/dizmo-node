var express 	= require('express')
var router 		= express.Router()
var crypto 		= require('crypto')
var User 		= require('../models/UserModel')
var Employee 	= require('../models/EmployeeModel')
var Employer 	= require('../models/EmployerModel')
var auth 		= require('../middlewares/auth')


// ---------------------------------------------------------
// *********************** USER ****************************
// ---------------------------------------------------------	

var deleteUser = function(req , res, ID){
    User.findOne({active : true, _id:ID }, function(err, user){
		if (err){
			res.status(500).json({
				success : false,
				message : "Internal Server Error. Profile Couldn't be Created."
			});
		} else{
			if (user){
				user.remove({ _id:ID },function(error){
					if (error){
						res.status(500).json({
							success : false,
							message : "Internal Server Error. User Profile Couldn't be Deleted."
						})
					} else{
						res.json({
							success : false,
							message : "User Profile Profile Deleted."
						})
					}
				})
			} else{
				res.status(404).json({
					success : false,
					message : "Doesn't Exists!"
				})
			}
		}
	})
}


/**
 * @api {post} /user/ Create New User
 * @apiName PostUser
 * @apiGroup User
 * @apiPermission None
 *
 * @apiParam {String} 	email 			Mandatory User's Email.
 * @apiParam {String} 	password 		Mandatory User's Password (Length at least 5).
 * @apiParam {String} 	role 			Mandatory User's Role.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.post('/', function(req, res) {
	if (req.body.email)
		email = req.body.email

	if (req.body.password)
		password = req.body.password

	if (req.body.role)
		role = req.body.role

	if (!req.body.password || password.length < 5 || !req.body.email || email.length < 1 || !req.body.role || role.length < 1){
		res.status(400).json({
			success : false,
			message : "E-mail and User Role can't be null and Password length must be at least 5!"
		})
	} else{
	    user = new User()
	    user.email = req.body.email
	    user.password = crypto.createHash("sha256").update(password).digest('hex')
	    user.role = req.body.role
	    user.secretID = crypto.createHash("sha256").update(email).digest('hex')
	    user.created = new Date()
	    user.updated = new Date()

	    user.save(function(error, user){
	        if (!error){
	        	if (role == "admin"){
	        		// Send Verification Mail
	        		res.json({
	        			success : true,
	        			message : "Admin User Profile Successfully Created."
	        		})
	        	}

	        	else if (role == "employee"){
	        		employee = new Employee()
	        		employee.userID = user._id
	        		employee.email = user.email
	        		employee.created = user.created
	        		employee.updated = user.updated

	        		employee.save(function(error2, employee){
	        			if(!error2){
	        				// Send Verification Mail
	        				res.json({
								success: true,
								message: "Successful Registration! Employee Profile Created."
							})
	        			} else{
	        				deleteUser(req, res, user._id)
	        			}
	        		})
	        	} 

	        	else if (role == "employer"){
		        	employer = new Employer()
	        		employer.userID = user._id
	        		employer.email = user.email
	        		employer.created = user.created
	        		employer.updated = user.updated

	        		employer.save(function(error2, employer){
	        			if(!error2){
	        				// Send Verification Mail
	        				res.json({
								success: true,
								message: "Successful Registration! Employer Profile Created."
							})
	        			} else{
	        				deleteUser(req, res, user._id)
	        			}
	        		})
		        }

		        else{
		        	deleteUser(req, res, user._id)
		        }
		    } else {
		        res.status(400).json({
					success: false,
					message: "Registration Failed! Email Already Exists."
				})
		    }
	    })
	}
})



/**
 * @api {get} /user/page/:pageNum Request All User information
 * @apiName GetUserList
 * @apiGroup User
 * @apiPermission Admin
 *
 * @apiParam {Number} pageNum 	Page Number For Pagination.
 * @apiParam {Number} pageSize 	Each Page's Size.
 *
 * @apiSuccess {String} 	_id			Users unique ID.
 * @apiSuccess {String} 	email 		Email of the User.
 * @apiSuccess {String} 	role  		Role of the User.
 * @apiSuccess {Boolean} 	created 	true/false.
 * @apiSuccess {Boolean} 	updated  	true/false.
 */

router.get('/page/:pageNum/:pageSize', function(req, res) {
	var lt = req.params.pageSize
	var skp = (req.params.pageNum-1)*lt
	var query = User.find({}).sort({active:-1, verified:-1, created:-1}).skip(skp).limit(lt)

	query.select('email role secretID verified active')
	query.exec(function(error,user){
		if (!error){
	        res.json({
				success: true,
				message: "User List.",
				user: user
			})
	    } else {
	        res.status(400).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api {put} /user/:id/:hid Verify User Email
 * @apiName VerifyUser
 * @apiGroup User
 * @apiPermission None
 *
 * @apiParam {String} id 	Users unique ID.
 * @apiParam {String} hid 	Verification Key.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.put('/verify/:id/:hid', function(req, res) {
	var ID = req.params.id
	var hID = req.params.hid

    if (!ID || ID.length != 24){
		res.status(400).json({
			success : false,
			message : "ID Required and It's length must be 24."
		})
	} else{
	    User.findOne({active : true, _id:ID }, function(err, user){
	    	if (err) {
	    		res.status(500).json({
					success: false,
					message: "Internal Server Error, Verification Failed!"
				})
	    	} else{
	    		if (!user){
		            res.status(404).json({
						success: false,
						message: "Doesn't Exists!"
					})
		        } else{
		        	if (user.secretID == hID)
		        		user.verified = true

		        	user.updated = new Date()
		        	user.save(function(error, user){
				        if (!error){
				            res.json({
								success: true,
								message: "Successfully Verified."
							})
				        } else {
				            res.status(500).json({
								success: false,
								message: "Internal Server Error, Verification Failed!"
							})
				        }
				    })
		        }
		    }
	    })
	}
})


/**
 * @api {delete} /user/:id Delete Specific User
 * @apiName DeleteUser
 * @apiGroup User
 * @apiPermission User
 *
 * @apiParam {String} id Users unique ID.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.delete('/:id', function(req, res) {
    var ID = req.params.id

    if (!ID || ID.length != 24){
		res.status(400).json({
			success : false,
			message : "ID Required and It's length must be 24."
		})
	} else{
	    User.findOne({active : true, _id:ID }, function(err, user){
	    	if (err) {
	    		res.status(500).json({
					success: false,
					message: "Internal Server Error."
				})
	    	}

	        if (!user){
	            res.status(404).json({
					success: false,
					message: "Doesn't Exists!"
				})
	        } else {
	        	user.active = false
	        	user.updated = new Date()
	        	user.save(function(error, user){
			        if (!error){
			            res.json({
							success: true,
							message: "Successfully Deleted"
						})
			        } else {
			            res.status(500).json({
							success: false,
							message: "Internal Server Error"
						})
			        }
			    })
	        }
	    })
	}
})


module.exports = router