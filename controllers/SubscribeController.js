var express 	= require('express')
var router 		= express.Router()
var Category 	= require('../models/CategoryModel')
var Empolyee 	= require('../models/EmployeeModel')
var Subscribe 	= require('../models/SubscribeModel')
var auth 		= require('../middlewares/auth')


// ---------------------------------------------------------
// ******************** SUBSCRIBE **************************
// ---------------------------------------------------------


/**
 * @api {post} /subscribe Create New Subscription
 * @apiName PostSubscribe
 * @apiGroup Subscribe
 * @apiPermission Employee
 *
 * @apiParam {String} 	employeeID		Mandatory Employee ID.
 * @apiParam {String} 	categoryID		Mandatory Category ID.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.post('/', function(req, res) {
	if (req.body.employeeID)
		employeeID = req.body.employeeID

	if (req.body.categoryID)
		categoryID = req.body.categoryID

    if (!req.body.employeeID || employeeID.length != 24 || !req.body.categoryID || categoryID.length != 24){
		res.status(400).json({
			success : false,
			message : "EmployeeID and CategoryID can't be null"
		})
	} else{
	    Empolyee.findOne({ _id:employeeID }, function(err,emp){
	    	if (err){
	    		res.status(500).json({
	    			success : false,
	    			message : "Internal Server Error!"
	    		})
	    	} else{
		    	if (!emp){
		    		res.status(404).json({
		    			success : false,
		    			message : "Provides a Valid Employee."
		    		})
		    	} else {
		    		Category.findOne({ _id:categoryID }, function(err,cat){
				    	if (err){
				    		res.status(500).json({
				    			success : false,
				    			message : "Internal Server Error!"
				    		})
				    	}

				    	if (!cat){
				    		res.status(404).json({
				    			success : false,
				    			message : "Provides a Valid Category."
				    		})
				    	} else {
				    		subscribe = new Subscribe()
				    		subscribe.employeeID = employeeID
				    		subscribe.categoryID = categoryID
						    subscribe.created = new Date()
						    subscribe.save(function(error){
						        if (!error){
						            res.json({
										success: true,
										message: "New Subscription created."
									})
						        } else {
						            res.status(500).json({
										success: false,
										message: "Internal Server Error! Combination of EmployeeID and CategoryID must be unique!"
									})
						        }
						    })
				    	}
				    })
	    		}
	    	}
	    })
	}
})


/**
 * @api {get} /subscribe/page/:pageNum Request All Subscription information
 * @apiName GetSubscriptionList
 * @apiGroup Subscribe
 * @apiPermission Admin
 *
 * @apiParam {Number} pageNum Page Number For Pagination.
 * @apiParam {Number} pageSize Each Page's Size.
 *
 * @apiSuccess {String} _id				Subscription Unique ID.
 * @apiSuccess {String} employeeID 		Empolyee ID.
 * @apiSuccess {String} categoryID		Category ID.
 */

router.get('/page/:pageNum/:pageSize', function(req, res) {
	var lt = req.params.pageSize
	var skp = (req.params.pageNum-1)*lt
	var query = Subscribe.find({}).sort({ employeeID:1}).skip(skp).limit(lt)

	query.select('employeeID categoryID')
	query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "List of Subscription.",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api {get} /subscribe/employee/:id Request All Subscription Information of an Employee
 * @apiName GetSubscriptionListofEmployee
 * @apiGroup Subscribe
 * @apiPermission Employee
 *
 * @apiParam {String} id Employee Unique ID Number.
 *
 * @apiParam {Number} pageNum Page Number For Pagination.
 * @apiParam {Number} pageSize Each Page's Size.
 *
 * @apiSuccess {String} _id				Subscription Unique ID.
 * @apiSuccess {String} employeeID 		Empolyee ID.
 * @apiSuccess {String} categoryID		Category ID.
 */

router.get('/employee/:id/page/:pageNum/:pageSize', function(req, res) {
	var ID = req.params.id
	var lt = req.params.pageSize
	var skp = (req.params.pageNum-1)*lt
	var query = Subscribe.find({ employeeID:ID }).skip(skp).limit(lt)

	query.select('employeeID categoryID')
	query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "List of Subscription of Given Empolyee.",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api {get} /subscribe/category/:id Request All Subscription Information of a Category
 * @apiName GetSubscriptionListofCategory
 * @apiGroup Subscribe
 * @apiPermission None
 *
 * @apiParam {String} id Category Unique ID Number.
 *
 * @apiParam {Number} pageNum Page Number For Pagination.
 * @apiParam {Number} pageSize Each Page's Size.
 *
 * @apiSuccess {String} _id				Subscription Unique ID.
 * @apiSuccess {String} employeeID 		Empolyee ID.
 * @apiSuccess {String} categoryID		Category ID.
 */

router.get('/category/:id/page/:pageNum/:pageSize', function(req, res) {
	var ID = req.params.id
	var lt = req.params.pageSize
	var skp = (req.params.pageNum-1)*lt
	var query = Subscribe.find({ categoryID:ID }).skip(skp).limit(lt)

	query.select('employeeID categoryID')
	query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "List of Subscription of Given Category.",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api {delete} /subscribe/:id Delete Specific Subscription
 * @apiName DeleteSubscription
 * @apiGroup Subscribe
 * @apiPermission Employee
 *
 * @apiParam {String} id Subscription unique ID.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.delete('/:id', function(req, res) {
    var ID = req.params.id
    
    Subscribe.findOne({ _id:ID }, function(err, data){
		if (err){
			res.status(500).json({
				success : false,
				message : "Internal Server Error"
			});
		} else{
			if (data){
				data.remove({ _id:ID },function(error){
					if (error){
						res.status(500).json({
							success : false,
							message : "Internal Server Error"
						})
					} else{
						res.json({
							success : true,
							message : "Successfully Deleted!"
						})
					}
				})
			} else{
				res.status(404).json({
					success : false,
					message : "Doesn't Exists!"
				})
			}
		}
	})
})


module.exports = router