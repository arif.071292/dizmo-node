var express 	= require('express')
var router 		= express.Router()
var Chat 		= require('../models/ChatModel')
var Employee 	= require('../models/EmployeeModel')
var Employer 	= require('../models/EmployerModel')
var auth 		= require('../middlewares/auth')


// Middlewares
router.use(function(req, res, next){
    if (auth.isAuthenticated(req, res, next)) {
        next();
    }
});


// ---------------------------------------------------------
// *********************** CHAT ****************************
// ---------------------------------------------------------


/**
 * @api 			{post} /chat 	Add New Message
 * @apiName 		PostChat
 * @apiGroup 		Chat
 * @apiPermission 	User
 *
 * @apiParam {String} 	message  	Mandatory Message Body
 * @apiParam {String} 	employerID	Mandatory Employer ID
 * @apiParam {String}   employeeID	Mandatory Employee ID
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.post('/', function(req, res) {
	if (req.body.employerID)
		employerID = req.body.employerID

	if (req.body.employeeID)
		employeeID = req.body.employeeID

	if (req.body.message)
		message = req.body.message

    if (!req.body.message || message.length < 1 || !req.body.employerID || employerID.length != 24 || !req.body.employeeID || employeeID.length != 24 || employerID == employeeID){
		res.status(400).json({
			success : false,
			message : "Chat Message, Empolyer ID and Employee ID can't be null"
		})
	} else{
	    Employer.findOne({ _id:employerID }, function(err,emp){
	    	if (err){
	    		res.status(500).json({
	    			success : false,
	    			message : "Internal Server Error!"
	    		})
	    	}

	    	if (!emp){
	    		res.status(404).json({
	    			success : false,
	    			message : "Provides a Valid Employer."
	    		})
	    	} else{
		    	Employee.findOne({ _id:employeeID }, function(err,emp){
			    	if (err){
			    		res.status(500).json({
			    			success : false,
			    			message : "Internal Server Error!"
			    		})
			    	}

			    	if (!emp){
			    		res.status(404).json({
			    			success : false,
			    			message : "Provides a Valid Employee."
			    		})
			    	} else{
				    	chat = new Chat()
				    	chat.employerID = employerID
				    	chat.employeeID = employeeID
				    	chat.message = message
					    chat.created = new Date()

					    chat.save(function(error){
					        if (!error){
					            res.json({
									success: true,
									message: "New Message Added."
								})
					        } else {
					            res.status(500).json({
									success: false,
									message: "Internal Server Error!"
								})
					        }
					    })
					}
			    })
			}
	    })
	}
})


/**
 * @api {get} /chat Request All Chat History
 * @apiName GetChatHistory
 * @apiGroup Chat
 * @apiPermission User
 *
 * @apiParam {String} id Employee/Employer ID.
 *
 * @apiSuccess {String} _id				Chat Unique ID.
 * @apiSuccess {String} message			Message.
 * @apiSuccess {String} employerID		Employer ID.
 * @apiSuccess {Date} 	employeeID		Employer ID.
 * @apiSuccess {Date} 	created  		Chat Created Date (Js Date Format).
 */

router.get('/user/:id', function(req, res) {
	var ID = req.params.id
	var query = Chat.find({$or:[{'employerID':ID}, {'employeeID':ID}]}).sort({employerID:1, employeeID:1, created:1})

	query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "Chat History of Given User.",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api {delete} /chat/:id Delete Specific Chat History
 * @apiName DeleteChat
 * @apiGroup Chat
 * @apiPermission User
 *
 * @apiParam {String} id Chat's unique ID.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.delete('/:id', function(req, res) {
    var ID = req.params.id

    if (!ID || ID.length != 24){
		res.status(400).json({
			success : false,
			message : "ID Required and It's length must be 24."
		})
	} else{
	    Chat.findOne({ _id:ID }, function(err, data){
			if (err){
				res.status(500).json({
					success : false,
					message : "Internal Server Error"
				});
			} else{
				if (data){
					data.remove({ _id:ID },function(error){
						if (error){
							res.status(500).json({
								success : false,
								message : "Internal Server Error"
							})
						} else{
							res.json({
								success : true,
								message : "Successfully Deleted!"
							})
						}
					})
				} else{
					res.status(404).json({
						success : false,
						message : "Doesn't Exists!"
					})
				}
			}
		})
	}
})


module.exports = router