var express 	= require('express')
var router 		= express.Router()
var Category 	= require('../models/CategoryModel')
var auth 		= require('../middlewares/auth')


// Middlewares
router.use(function(req, res, next){
    if (auth.isAuthenticated(req, res, next)) {
        next();
    }
});


// ---------------------------------------------------------
// ********************* CATEGORY **************************
// ---------------------------------------------------------


/**
 * @api 			{post} /category 	Create New Category
 * @apiName 		PostCategory
 * @apiGroup 		Category
 * @apiPermission 	Admin
 *
 * @apiParam 		{String} 	Name 		Mandatory Category Name.
 *
 * @apiSuccess 		{Boolean} 	success		true/false.
 * @apiSuccess 		{String} 	message 	success/error message.
 */

router.post('/', function(req, res, next) {
	if (req.body.name)
		name = req.body.name

    if (!req.body.name || name.length < 1){
		res.status(400).json({
			success : false,
			message : "Category name can't be null."
		})
	} else{
	    category = new Category()
	    category.name = name
	    category.created = new Date()
	    category.updated = new Date()

	    category.save(function(error, data){
	        if (!error){
	            res.json({
					success: true,
					id 	   : data._id,
					message: "New Job Category Added."
				})
	        } else {
	            res.status(500).json({
					success: false,
					message: "Internal Server Error! Category name must be unique!"
				})
	        }
	    })
	}
})


/**
 * @api 			{get} /category/ 	Request All Category List
 * @apiName 		GetCategoryList
 * @apiGroup 		Category
 * @apiPermission 	None
 *
 * @apiSuccess {String} 	_id		Category Unique ID.
 * @apiSuccess {String} 	name 	Name of the Category.
 */

router.get('/', function(req, res) {
	var query = Category.find({}).sort({name: 1}).select('name')

	query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "Category List",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api {get} 		/category/:id 	Request a Specific Category Info
 * @apiName 		GetCategory
 * @apiGroup 		Category
 * @apiPermission 	None
 *
 * @apiParam 		{String} 	id 		Category Unique ID.
 *
 * @apiSuccess 		{String} 	_id		Category Unique ID.
 * @apiSuccess 		{String} 	name 	Name of the Category.
 */

router.get('/:id', function(req, res) {
	var ID = req.params.id

    if (!ID || ID.length != 24){
		res.status(400).json({
			success : false,
			message : "ID Required and It's length must be 24."
		})
	} else{
		var query = Category.findOne({ _id:ID }).select('name')

		query.exec(function(error,data){
			if (!error){
				if (data){
				    res.json({
						success: true,
						message: "Category name of given ID",
						data: data
					})
			    } else {
			        res.status(404).json({
						success: false,
						message: "Category Doesn't Exists!!"
					})
			    }
	        } else {
				res.status(500).json({
					success: false,
					message: "Internal Server Error!"
				})
	        }
		})
	}
})


/**
 * @api 			{put} /category/:id 	Update Specific Category
 * @apiName 		UpdateCategory
 * @apiGroup 		Category
 * @apiPermission 	Admin
 *
 * @apiParam 		{String} 	id 			Category unique ID.
 * @apiParam 		{String} 	[name] 		Optional Category Name.
 *
 * @apiSuccess 		{Boolean} 	success		true/false.
 * @apiSuccess 		{String} 	message 	success/error message.
 */

router.put('/:id', function(req, res) {
	var ID = req.params.id

	if (req.body.name)
		name = req.body.name

    if (!ID || ID.length != 24){
		res.status(400).json({
			success : false,
			message : "ID Required and It's length must be 24."
		})
	} else{
	    Category.findOne({ _id:ID }, function(err, category){
	    	if (err) {
	    		res.status(500).json({
					success: false,
					message: "Internal Server Error, Update Failed!"
				})
	    	}

	        if (!category) {
	            res.status(404).json({
					success: false,
					message: "Doesn't Exists!"
				})
	        } else {
	        	if (name && name.length > 0)
	        		category.name = name

	        	category.update = new Date()
	        	category.save(function(error){
			        if (!error){
			            res.json({
							success: true,
							message: "Successfully Updated."
						})
			        } else {
			            res.status(500).json({
							success: false,
							message: "Internal Server Error, Update Failed!"
						})
			        }
			    })
	        }
	    })
	}
})


/**
 * @api 			{delete} /category/:id 	Delete Specific Category
 * @apiName 		DeleteCategory
 * @apiGroup 		Category
 * @apiPermission 	Admin
 *
 * @apiParam 		{String} 	id 		Category unique ID.
 *
 * @apiSuccess 		{Boolean} 	success		true/false.
 * @apiSuccess 		{String} 	message 	success/error message.
 */

router.delete('/:id', function(req, res) {
	var ID = req.params.id
    
    Category.findOne({ _id:ID }, function(err, data){
		if (err){
			res.status(500).json({
				success : false,
				message : "Internal Server Error"
			});
		} else{
			if (data){
				data.remove({ _id:ID },function(error){
					if (error){
						res.status(500).json({
							success : false,
							message : "Internal Server Error"
						})
					} else{
						res.json({
							success : true,
							message : "Successfully Deleted!"
						})
					}
				})
			} else{
				res.status(404).json({
					success : false,
					message : "Doesn't Exists!"
				})
			}
		}
	})
})


module.exports = router