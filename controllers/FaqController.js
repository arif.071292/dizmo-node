var express = require('express')
var router 	= express.Router()
var Faq 	= require('../models/FaqModel')
var auth 	= require('../middlewares/auth')


// Middlewares
router.use(function(req, res, next){
    if (auth.isAuthenticated(req, res, next)) {
        next();
    }
});


// ---------------------------------------------------------
// ************************ FAQ ****************************
// ---------------------------------------------------------


 /**
 * @api {post} /faq Add New Questions
 * @apiName PostFaq
 * @apiGroup Faq
 * @apiPermission Admin
 *
 * @apiParam {String} 	question 	Mandatory Question.
 * @apiParam {String} 	answer 		Mandatory Answer to the Question.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.post('/', function(req, res) {
	if (req.body.question)
		question = req.body.question

	if (req.body.answer)
		answer = req.body.answer

    if (!req.body.question || question.length < 1 || !req.body.answer || answer.length < 1){
		res.status(400).json({
			success : false,
			message : "Question and Answer can't be null"
		})
	} else{
		faq = new Faq()
		faq.question = question
		faq.answer = answer
	    faq.created = new Date()
	    faq.updated = new Date()

	    faq.save(function(error){
	        if (!error){
	            res.json({
					success: true,
					message: "New Question and Answer Added."
				})
	        } else {
	            res.status(500).json({
					success: false,
					message: "Internal Server Error! Each Question Must Be Unique."
				})
	        }
	    })
	}
})


/**
 * @api {get} /faq Request All FAQ
 * @apiName GetFaqList
 * @apiGroup Faq
 * @apiPermission None
 *
 * @apiSuccess {String} _id				FAQ Unique ID.
 * @apiSuccess {String} question		Frequently Asked Question.
 * @apiSuccess {String} answer			Answers to the Questions
 */

router.get('/', function(req, res) {
	Faq.find({}).exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "Frequently Asked Questions.",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api {put} /faq/:id Update Specific Question
 * @apiName UpdateFaq
 * @apiGroup Faq
 * @apiPermission Admin
 *
 * @apiParam {String} id Question's unique ID.
 *
 * @apiParam {String} 	[question] 	Question.
 * @apiParam {String} 	[answer] 	Answer to the Question.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.put('/:id', function(req, res) {
	var ID = req.params.id

	if (req.body.question)
		question = req.body.question

	if (req.body.answer)
		answer = req.body.answer

    if (!ID || ID.length != 24){
		res.status(400).json({
			success : false,
			message : "ID Required and It's length must be 24."
		})
	} else{
	   	Faq.findOne({ _id:ID }, function(err, faq) {
	   		if (err) {
	   			res.status(500).json({
					success: false,
					message: "Internal Server Error, Update Failed!"
				})
	   		}

	        if (!faq){
	            res.status(404).json({
					success: false,
					message: "Doesn't Exists!"
				})
	        } else {
	        	if (req.body.question && question.length > 0)
	        		faq.question = question

	        	if (req.body.answer && answer.length > 0)
	        		faq.answer = answer
	        	
	        	faq.update = new Date()
	        	faq.save(function(error){
			        if (!error){
			            res.json({
							success: true,
							message: "Successfully Updated."
						})
			        } else {
			            res.status(500).json({
							success: false,
							message: "Internal Server Error, Update Failed!"
						})
			        }
			    })
	        }
	    })
	}
})


/**
 * @api {delete} /faq/:id Delete Specific Question
 * @apiName DeleteFaq
 * @apiGroup Faq
 * @apiPermission Admin
 *
 * @apiParam {String} id Question's unique ID.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.delete('/:id', function(req, res) {
    var ID = req.params.id
    
    Faq.findOne({ _id:ID }, function(err, data){
		if (err){
			res.status(500).json({
				success : false,
				message : "Internal Server Error"
			})
		} else{
			if (data){
				data.remove({ _id:ID },function(error){
					if (error){
						res.status(500).json({
							success : false,
							message : "Internal Server Error"
						})
					} else{
						res.json({
							success : true,
							message : "Successfully Deleted!"
						})
					}
				})
			} else{
				res.status(404).json({
					success : false,
					message : "Doesn't Exists!"
				})
			}
		}
	})
})


module.exports = router