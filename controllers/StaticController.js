var express = require('express')
var router 	= express.Router()
var Static 	= require('../models/StaticModel')
var auth 	= require('../middlewares/auth')


// Middlewares
router.use(function(req, res, next){
    if (auth.isAuthenticated(req, res, next)) {
        next();
    }
});


// ---------------------------------------------------------
// *********************** STATIC **************************
// ---------------------------------------------------------


/**
 * @api {post} /static Add New Static Pages
 * @apiName PostStatic
 * @apiGroup Static
 * @apiPermission Admin
 *
 * @apiParam {String} 	attribute		Mandatory Attribute Name.
 * @apiParam {String} 	description		Mandatory Attribute Description.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.post('/', function(req, res) {
	if (req.body.attribute)
		attribute = req.body.attribute

	if (req.body.description)
		description = req.body.description

    if (!req.body.attribute || attribute.length < 1 || !req.body.description || description.length < 1){
		res.status(400).json({
			success : false,
			message : "Attribute and It's Descriptions can't be null"
		})
	} else{
	    static = new Static()

	    static.attribute.en = attribute
	    static.description.en = description
	    static.attribute.bn = attribute
	    static.description.bn = description
	    static.created = new Date()
	    static.updated = new Date()

	    static.save(function(error, data){
	        if (!error){
	            res.json({
					success: true,
					message: "New Static Element Added."
				})
	        } else {
	            res.status(500).json({
					success: false,
					message: "Internal Server Error! Each Attribute Must Be Unique."
				})
	        }
	    })
	}
})


/**
 * @api {get} /static Request All Static Elements
 * @apiName GetStaticList
 * @apiGroup Static
 * @apiPermission None
 *
 * @apiSuccess {String} _id				Static Page ID.
 * @apiSuccess {String} attribute		Static Page Name.
 */

router.get('/', function(req, res) {
	var query = Static.find({}).sort({attribute:1}).select('attribute')

	query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "Static Pages.",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api {get} /static/:id Request Given Static Elements
 * @apiName GetStatic
 * @apiGroup Static
 * @apiPermission None
 *
 * @apiParam {String} id Static Page ID.
 *
 * @apiSuccess {String} _id				Static Page ID.
 * @apiSuccess {String} attribute		Static Page Name.
 * @apiSuccess {String} description		Static Page description.
 */

router.get('/:id', function(req, res) {
	var ID = req.params.id

   	Static.findOne({ _id:ID }, function(err, data){
		if (!err){
			if (data){
			    res.json({
					success: true,
					message: "Specific Static Pages Descriptions.",
					data: data
				})
		    } else {
		        res.status(404).json({
					success: false,
					message: "Doesn't Exists!"
				})
		    }
        } else {
        	res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
        }
	})
})


/**
 * @api {put} /static/:id Update Specific Static Element
 * @apiName UpdateStatic
 * @apiGroup Static
 * @apiPermission Admin
 *
 * @apiParam {String} id Attribute's unique ID
 *
 * @apiParam {String} 	[attribute]		Attribute Name.
 * @apiParam {String} 	[description]	Description.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.put('/:id', function(req, res) {
	var ID = req.params.id

    if (!ID || ID.length != 24 || !req.body.language){
		res.status(400).json({
			success : false,
			message : "ID Required and It's length must be 24. Language Format is Also Required."
		})
	} else{
	    Static.findOne({ _id:ID }, function(err, static){
	    	if (err) {
	    		res.status(500).json({
					success: false,
					message: "Internal Server Error, Update Failed!"
				})
	    	}

	        if (!static) {
	            res.status(404).json({
					success: false,
					message: "Doesn't Exists!"
				})
	        } else {
	        	if (req.body.attribute && req.body.attribute.length > 0)
	        		if (req.body.language == 'bn') {
	        			static.attribute.bn = req.body.attribute
	        		} else{
	        			static.attribute.en = req.body.attribute
	        		}

	        	if (req.body.description && req.body.description.length > 0)
	        		if (req.body.language == 'bn') {
	        			static.description.bn = req.body.description
	        		} else{
	        			static.description.en = req.body.description
	        		}

	        	static.update = new Date()

	        	static.save(function(error){
			        if (!error){
			            res.json({
							success: true,
							message: "Successfully Updated."
						})
			        } else {
			            res.status(500).json({
							success: false,
							message: "Internal Server Error, Update Failed!"
						})
			        }
			    })
	        }
	    })
	}
})


/**
 * @api {delete} /static/:id Delete Specific Static Element
 * @apiName DeleteStatic
 * @apiGroup Static
 * @apiPermission Admin
 *
 * @apiParam {String} id Attribute's unique ID.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.delete('/:id', function(req, res) {
    var ID = req.params.id
    
    Static.findOne({ _id:ID }, function(err, data){
		if (err){
			res.status(500).json({
				success : false,
				message : "Internal Server Error"
			});
		} else{
			if (data){
				data.remove({ _id:ID },function(error){
					if (error){
						res.status(500).json({
							success : false,
							message : "Internal Server Error"
						})
					} else{
						res.json({
							success : true,
							message : "Successfully Deleted!"
						})
					}
				})
			} else{
				res.status(404).json({
					success : false,
					message : "Doesn't Exists!"
				})
			}
		}
	})
})


module.exports = router