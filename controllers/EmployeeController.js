var express 	= require('express')
var router 		= express.Router()
var Employee 	= require('../models/EmployeeModel')
var auth 		= require('../middlewares/auth')


// Middlewares
router.use(function(req, res, next){
    if (auth.isAuthenticated(req, res, next)) {
        next();
    }
});


// ---------------------------------------------------------
// ******************** Employee ***************************
// ---------------------------------------------------------


/**
 * @api {get} /employee/page/:pageNum Request All Employee information
 * @apiName GetEmployeeList
 * @apiGroup Employee
 * @apiPermission Admin
 *
 * @apiParam {Number} pageNum Page Number For Pagination.
 * @apiParam {Number} pageSize Each Page's Size.
 *
 * @apiSuccess {String} _id				Employee's unique ID.
 * @apiSuccess {String} userID 			Employee's unique User ID.
 * @apiSuccess {String} email 			Email of the Employee.
 */

router.get('/page/:pageNum/:pageSize', function(req, res) {
	var lt = req.params.pageSize
	var skp = (req.params.pageNum-1)*lt
	var query = Employee.find({}).sort({created:-1}).skip(skp).limit(lt).select('userID email')

	query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "Employee List.",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api {get} /employee/:id Request a Specific Employee information
 * @apiName GetEmployee
 * @apiGroup Employee
 * @apiPermission User
 *
 * @apiParam {String} id Employee's unique ID.
 *
 * @apiSuccess {String} _id				Employee's unique ID.
 * @apiSuccess {String} firstname 		firstname of the Employee (optional).
 * @apiSuccess {String} lastname  		lastname of the Employee (optional).
 * @apiSuccess {String} userID 			Employee's unique User ID.
 * @apiSuccess {String} email 			Email of the Employee.
 * @apiSuccess {String} website  		Website of the Employee (optional).
 * @apiSuccess {String} description		Description about the Employee (optional).
 * @apiSuccess {String} country  		Country of the Employee (optional).
 * @apiSuccess {URL} 	coverphoto 		URL of Cover Photo of the Employee (optional).
 * @apiSuccess {URL} 	propic 			URL of Profile Pictures of the Employee (optional).
 * @apiSuccess {URL} 	gender			Gender of the Employee (optional).
 * @apiSuccess {Date} 	created 		Employee Creation Date (Js Date Format).
 * @apiSuccess {Date} 	updated  		Info Update Date (Js Date Format).
 */

router.get('/:id', function(req, res) {
	var ID = req.params.id
	var query = Employee.findOne({ _id:ID })

	query.exec(function(error,data){
		if (!error){
	        res.json({
				success: true,
				message: "Employee of Given ID",
				data: data
			})
	    } else {
	        res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
	    }
	})
})


/**
 * @api {put} /employee/:id Update Specific Employee
 * @apiName UpdateEmployee
 * @apiGroup Employee
 * @apiPermission Employee
 *
 * @apiParam {String} id Employee's unique ID.
 *
 * @apiParam {String} 	[firstname] 	firstname of the Employee.
 * @apiParam {String} 	[lastname] 		lastname of the Employee.
 * @apiParam {String} 	[website] 		Website of the Employee.
 * @apiParam {String} 	[description]	Description about the Employee.
 * @apiParam {String} 	[country]  		Country of the Employee.
 * @apiParam {URL} 		[coverphoto] 	URL of Cover Photo of the Employee.
 * @apiParam {URL} 		[propic] 		URL of Profile Pictures of the Employee.
 * @apiParam {URL} 		[gender]		Gender of the Employee.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.put('/:id', function(req, res) {
	var ID = req.params.id

    if (!ID || ID.length != 24){
		res.status(400).json({
			success : false,
			message : "ID Required and It's length must be 24."
		})
	} else{
		Employee.findOne({ _id:ID }, function(err, employee){
			if (err) {
				res.status(500).json({
					success: false,
					message: "Internal Server Error, Update Failed!"
				})
			}
	        if (!employee){
	            res.status(404).json({
					success: false,
					message: "Doesn't Exists!"
				})
	        } else {
	        	if (req.body.firstname)
	        		employee.firstname = req.body.firstname

	        	if (req.body.lastname)
	        		employee.lastname = req.body.lastname

	        	if (req.body.website)
	        		employee.website = req.body.website

	        	if (req.body.description)
	        		employee.description = req.body.description

	        	if (req.body.coverphoto)
	        		employee.coverphoto = req.body.coverphoto

	        	if (req.body.propic)
	        		employee.propic = req.body.propic

	        	if (req.body.country)
	        		employee.country = req.body.country

	        	if (req.body.gender)
	        		employee.gender = req.body.gender

	        	employee.updated = new Date()
	        	employee.save(function(error, data){
			        if (!error){
			            res.json({
							success: true,
							message: "Successfully Updated."
						})
			        } else {
			            res.status(500).json({
							success: false,
							message: "Internal Server Error, Update Failed!"
						})
			        }
			    })
	        }
	    })
	}
})


module.exports = router