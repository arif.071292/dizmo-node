var express 	= require('express')
var router 		= express.Router()
var Job 		= require('../models/JobModel')
var Employer 	= require('../models/EmployerModel')
var Category 	= require('../models/CategoryModel')
var SubCategory = require('../models/SubCategoryModel')
var auth 		= require('../middlewares/auth')


// Middlewares
router.use(function(req, res, next){
    if (auth.isAuthenticated(req, res, next)) {
        next();
    }
});


// ---------------------------------------------------------
// *********************** JOB ****************************
// ---------------------------------------------------------


/**
 * @api {post} /job Create New Job
 * @apiName PostJob
 * @apiGroup Job
 * @apiPermission Employer
 *
 * @apiParam {String} 	title 			Mandatory Job's Title.
 * @apiParam {String} 	categoryID 		Mandatory Job Category ID.
 * @apiParam {String} 	subcategoryID	Mandatory Job Sub-Category ID.
 * @apiParam {String} 	employerID		Mandatory Employer ID.
 * @apiParam {String} 	[description] 	Job's Descripton.
 * @apiParam {Date} 	[deadline]		Deadline of the Job Application.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.post('/', function(req, res) {
	if (req.body.title)
		title = req.body.title

	if (req.body.categoryID)
		catID = req.body.categoryID

	if (req.body.subcategoryID)
		subcatID = req.body.subcategoryID

	if (req.body.employerID)
		empID = req.body.employerID

    if (!req.body.title || title.length < 1 || !req.body.categoryID || catID.length != 24 || !req.body.subcategoryID || subcatID.length != 24 || !req.body.employerID || empID.length != 24 ){
		res.status(400).json({
			success : false,
			message : "Title, Category ID, Subcategory ID, Employer ID can't be null and All ID length must be 24."
		})
	} else{
	   	Category.findOne({ _id:catID }, function(err,cat){
	   		if(err){
	   			res.status(500).json({
	   				success : false,
	   				message : "Internal Server Error."
	   			})
	   		} else{
	   			if (!cat){
		   			res.status(404).json({
		   				success : false,
		   				message : "Provides a Valid Category."
		   			})
		   		} else{
		   			SubCategory.findOne({ _id:subcatID }, function(err,subcat){
				   		if(err){
				   			res.status(500).json({
				   				success : false,
				   				message : "Internal Server Error."
				   			})
				   		}

				   		if (!subcat || catID != subcat.categoryID){
				   			res.status(404).json({
				   				success : false,
				   				message : "Provides a Valid Sub-Category."
				   			})
				   		} else{
				   			Employer.findOne({ _id:empID }, function(err, emp){
						   		if(err){
						   			res.status(500).json({
						   				success : false,
						   				message : "Internal Server Error."
						   			})
						   		}

						   		if (!emp){
						   			res.status(404).json({
						   				success : false,
						   				message : "Provides a Valid Employer."
						   			})
						   		} else{
						   			job = new Job()
						   			job.title = title
						   			job.category = cat.name
						   			job.subcategory = subcat.name
						   			job.employerName = emp.name
						   			job.employerMail = emp.email

						   			if (req.body.description)
								   		job.description = req.body.description
								   	if (req.body.deadline)
								   		job.deadline = req.body.deadline

								    job.created = new Date()
								    job.updated = new Date()


								    job.save(function(error, data){
								        if (!error){
								            res.json({
												success: true,
												message: "New Job created."
											})
								        } else {
								            res.status(500).json({
												success: false,
												message: "Internal Server Error! Combination of Title, Category and Employer Name must be unique!"
											})
								        }
								    })
						   		}
						   	})
				   		}
				   	})
		   		}
	   		}
	   	})
	}
})


/**
 * @api {get} /job/page/:pageNum Request All Job information
 * @apiName GetJobList
 * @apiGroup Job
 * @apiPermission None
 *
 * @apiParam {Number} pageNum 	Page Number For Pagination.
 * @apiParam {Number} pageSize 	Each Page's Size.
 *
 *
 * @apiSuccess {String} 	_id				Job Unique ID.
 * @apiSuccess {String} 	title  			Job Title.
 * @apiSuccess {String} 	category 		Job's Category Name.
 * @apiSuccess {String} 	subcategory		Job's Sub-Category Name.
 * @apiSuccess {String} 	employerName  	Employer Name.
 * @apiSuccess {String} 	employerMail  	Employer E-mail.
 * @apiSuccess {String} 	description		Job Description optional).
 * @apiSuccess {String} 	deadline  		Job Deadline(optional).
 * @apiSuccess {Boolean} 	featured		true/false.
 * @apiSuccess {Boolean} 	active 			true/false.
 * @apiSuccess {Date} 		created 		Job Creation Date (Js Date Format).
 * @apiSuccess {Date} 		updated  		Info Update Date (Js Date Format).
 */

router.get('/page/:pageNum/:pageSize', function(req, res) {
	var lt = req.params.pageSize
	var skp = (req.params.pageNum-1)*lt
	var query = Job.find({active : true}).sort({featured:1, created:-1}).skip(skp).limit(lt)

	query.exec(function(error,data){
		if (error){
			res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
        } else{
        	res.json({
				success: true,
				message: "Job's List",
				data: data
			})
        }
	})
})


/**
 * @api {get} /job/category/:id Request Job information of a Specific Category
 * @apiName GetJobofCategory
 * @apiGroup Job
 * @apiPermission None
 *
 * @apiParam {id} id Category Unique ID.
 *
 * @apiSuccess {String} 	_id				Job Unique ID.
 * @apiSuccess {String} 	title  			Job Title.
 * @apiSuccess {String} 	category 		Job's Category Name.
 * @apiSuccess {String} 	subcategory		Job's Sub-Category Name.
 * @apiSuccess {String} 	employerName  	Employer Name.
 * @apiSuccess {String} 	employerMail  	Employer E-mail.
 * @apiSuccess {String} 	description		Job Description optional).
 * @apiSuccess {String} 	deadline  		Job Deadline(optional).
 * @apiSuccess {Boolean} 	featured		true/false.
 * @apiSuccess {Boolean} 	active 			true/false.
 * @apiSuccess {Date} 		created 		Job Creation Date (Js Date Format).
 * @apiSuccess {Date} 		updated  		Info Update Date (Js Date Format).
 */

router.get('/category/:id', function(req, res) {
	var ID = req.params.id

	Category.findOne({ _id:ID }, function(err,cat){
	   	if(err){
	   		res.status(500).json({
	   			success : false,
	   			message : "Internal Server Error."
	   		})
	   	} else {
		   	if (cat){
		   		var query = Job.find({active : true, category : cat.name}).sort({featured:1, created:-1})

			    query.exec(function(error,data){
					if (error){
						res.status(500).json({
							success: false,
							message: "Internal Server Error!"
						})
			        }

					res.json({
						success: true,
						message: "All Jobs of Given Category.",
						data: data
					})
				})
		   	} else{
		   		res.status(404).json({
		   			success : false,
		   			message : "Provides a Valid Category."
		   		})
		   	}
		}
	})
})


/**
 * @api {get} /job/subcategory/:id Request Job information of a Specific SubCategory
 * @apiName GetJobofSubCategory
 * @apiGroup Job
 * @apiPermission None
 *
 * @apiParam {id} id Sub-Category Unique ID.
 *
 * @apiSuccess {String} 	_id				Job Unique ID.
 * @apiSuccess {String} 	title  			Job Title.
 * @apiSuccess {String} 	category 		Job's Category Name.
 * @apiSuccess {String} 	subcategory		Job's Sub-Category Name.
 * @apiSuccess {String} 	employerName  	Employer Name.
 * @apiSuccess {String} 	employerMail  	Employer E-mail.
 * @apiSuccess {String} 	description		Job Description optional).
 * @apiSuccess {String} 	deadline  		Job Deadline(optional).
 * @apiSuccess {Boolean} 	featured		true/false.
 * @apiSuccess {Boolean} 	active 			true/false.
 * @apiSuccess {Date} 		created 		Job Creation Date (Js Date Format).
 * @apiSuccess {Date} 		updated  		Info Update Date (Js Date Format).
 */

router.get('/subcategory/:id', function(req, res) {
	var ID = req.params.id
	var subcatname = ''

	SubCategory.findOne({ _id:ID }, function(err,subcat){
	   	if(err){
	   		res.status(500).json({
	   			success : false,
	   			message : "Internal Server Error."
	   		})
	   	}

	   	if (subcat){
		    var query = Job.find({active : true, subcategory : subcat.name}).sort({featured:1, created:-1})
		    query.exec(function(error,data){
				if (error){
					res.status(500).json({
						success: false,
						message: "Internal Server Error!"
					})
		        }

				res.json({
					success: true,
					message: "All Jobs of Given Sub-Category.",
					data: data
				})
			})
	   	} else{
	   		res.status(404).json({
	   			success : false,
	   			message : "Provides a Valid Sub-Category."
	   		})
	   	}
	})
})


/**
 * @api {get} /job/employer/:id Request Job information of a Specific Employer
 * @apiName GetJobofEmployer
 * @apiGroup Job
 * @apiPermission None
 *
 * @apiParam {id} id Employer Unique ID.
 *
 * @apiSuccess {String} 	_id				Job Unique ID.
 * @apiSuccess {String} 	title  			Job Title.
 * @apiSuccess {String} 	category 		Job's Category Name.
 * @apiSuccess {String} 	subcategory		Job's Sub-Category Name.
 * @apiSuccess {String} 	employerName  	Employer Name.
 * @apiSuccess {String} 	employerMail  	Employer E-mail.
 * @apiSuccess {String} 	description		Job Description optional).
 * @apiSuccess {String} 	deadline  		Job Deadline(optional).
 * @apiSuccess {Boolean} 	featured		true/false.
 * @apiSuccess {Boolean} 	active 			true/false.
 * @apiSuccess {Date} 		created 		Job Creation Date (Js Date Format).
 * @apiSuccess {Date} 		updated  		Info Update Date (Js Date Format).
 */

router.get('/employer/:id', function(req, res) {
	var ID = req.params.id
	var email = ''

	Employer.findOne({ _id:ID }, function(err,emp){
	   	if(err){
	   		res.status(500).json({
	   			success : false,
	   			message : "Internal Server Error."
	   		})
	   	} else{
		   	if (emp){
		   		var query = Job.find({active : true, employerMail : emp.email}).sort({featured:1, created:-1})
			    query.exec(function(error,data){
					if (error){
						res.status(500).json({
							success: false,
							message: "Internal Server Error!"
						})
			        }

					res.json({
						success: true,
						message: "All Jobs of Given Employer.",
						data: data
					})
				})
		   	} else{
		   		res.status(404).json({
		   			success : false,
		   			message : "Provides a Valid Employer."
		   		})
		   	}
		}
	})
})


/**
 * @api {get} /job/:id Request a Specific Job information
 * @apiName GetJob
 * @apiGroup Job
 * @apiPermission None
 *
 * @apiParam {id} id Job Unique ID.
 *
 * @apiSuccess {String} 	_id				Job Unique ID.
 * @apiSuccess {String} 	title  			Job Title.
 * @apiSuccess {String} 	category 		Job's Category Name.
 * @apiSuccess {String} 	subcategory		Job's Sub-Category Name.
 * @apiSuccess {String} 	employerName  	Employer Name.
 * @apiSuccess {String} 	employerMail  	Employer E-mail.
 * @apiSuccess {String} 	description		Job Description optional).
 * @apiSuccess {String} 	deadline  		Job Deadline(optional).
 * @apiSuccess {Boolean} 	featured		true/false.
 * @apiSuccess {Boolean} 	active 			true/false.
 * @apiSuccess {Date} 		created 		Job Creation Date (Js Date Format).
 * @apiSuccess {Date} 		updated  		Info Update Date (Js Date Format).
 */

router.get('/:id', function(req, res) {
	var ID = req.params.id

	Job.findOne({active : true, _id:ID }, function(error, data){
		if (!error){
			if (data){
			    res.json({
					success: true,
					message: "Job Descriptions of given ID",
					data: data
				})
		    } else {
		        res.status(404).json({
					success: false,
					message: "Job Doesn't Exists!!"
				})
		    }
        } else {
			res.status(500).json({
				success: false,
				message: "Internal Server Error!"
			})
        }
	})
})


/**
 * @api {get} /search/:keyword	 Request a Specific Job information
 * @apiName GetSearchResults
 * @apiGroup Job
 * @apiPermission None
 *
 * @apiParam {id} id Job Unique ID.
 *
 * @apiSuccess {String} 	_id				Job Unique ID.
 * @apiSuccess {String} 	title  			Job Title.
 * @apiSuccess {String} 	category 		Job's Category Name.
 * @apiSuccess {String} 	subcategory		Job's Sub-Category Name.
 * @apiSuccess {String} 	employerName  	Employer Name.
 * @apiSuccess {String} 	employerMail  	Employer E-mail.
 * @apiSuccess {String} 	description		Job Description optional).
 * @apiSuccess {String} 	deadline  		Job Deadline(optional).
 * @apiSuccess {Boolean} 	featured		true/false.
 * @apiSuccess {Boolean} 	active 			true/false.
 * @apiSuccess {Date} 		created 		Job Creation Date (Js Date Format).
 * @apiSuccess {Date} 		updated  		Info Update Date (Js Date Format).
 */

router.get('/search/:keyword', function(req, res) {
	var keyword = req.params.keyword

	Job.search({query_string: {query: keyword}}, {hydrate:true}, function(err, results) {
		if (err){
			res.status(500).json({
				success : false,
				message : "Internal Server Error!"
			})
		} else{
			res.json({
				success : true,
				message : "Search Results.",
				data 	: results
			})
		}
	})
})


/**
 * @api {put} /job/:id Update Specific Job
 * @apiName UpdateJob
 * @apiGroup Job
 * @apiPermission Employer
 *
 * @apiParam {String} id Job's unique ID.
 *
 * @apiParam {String} 	[title]			Job's Title.
 * @apiParam {String} 	[description] 	Job's Descripton.
 * @apiParam {Date} 	[deadline]		Deadline of the Job Application.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */


router.put('/:id', function(req, res) {
	var ID = req.params.id

    if (!ID || ID.length != 24){
		res.status(400).json({
			success : false,
			message : "ID Required and It's length must be 24."
		})
	} else{
	    Job.findOne({active : true, _id:ID }, function(err, job){
	    	if (err) {
	    		res.status(500).json({
					success: false,
					message: "Internal Server Error, Update Failed!"
				})
	    	}

	        if (!job) {
	            res.status(404).json({
					success: false,
					message: "Doesn't Exists!"
				})
	        } else {
	        	if (req.body.title && req.body.title.length > 0)
	        		job.title = req.body.title

	        	if (req.body.description)
	        		job.description = req.body.description

				if (req.body.featured)
	        		job.featured = req.body.featured
	        	
	        	if (req.body.deadline)
	        		job.deadline = req.body.deadline

	        	job.update = new Date()

	        	job.save(function(error){
			        if (!error){
			            res.json({
							success: true,
							message: "Successfully Updated."
						})
			        } else {
			            res.status(500).json({
							success: false,
							message: "Internal Server Error, Update Failed!"
						})
			        }
			    })
	        }
	    })
	}
})


/**
 * @api {delete} /job/:id Delete Specific Job
 * @apiName DeleteJob
 * @apiGroup Job
 * @apiPermission Employer
 *
 * @apiParam {String} id Job's unique ID.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.delete('/:id', function(req, res) {
    var ID = req.params.id

    if (!ID || ID.length != 24){
		res.status(400).json({
			success : false,
			message : "ID Required and It's length must be 24."
		})
	} else{
	    Job.findOne({active : true, _id:ID }, function(err, job){
	    	if (err) {
	    		res.status(500).json({
					success: false,
					message: "Internal Server Error"
				})
	    	}

	        if (!job) {
	            res.status(404).json({
					success: false,
					message: "Doesn't Exists!"
				})
	        }

	        job.active = false
	        job.save(function(error){
			    if (!error){
			        res.json({
						success: true,
						message: "Successfully Deleted."
					})
			    } else {
			        res.status(500).json({
						success: false,
						message: "Internal Server Error."
					})
			    }
			})
	    })
	}
})


module.exports = router