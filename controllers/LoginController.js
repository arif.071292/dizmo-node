var express = require('express')
var router 	= express.Router()
var crypto 	= require('crypto')
var User 	= require('../models/UserModel')


// ---------------------------------------------------------
// *********************** LOGIN ***************************
// ---------------------------------------------------------					


/**
 * @api {post} /login Log In
 * @apiName LogIN
 * @apiGroup User
 * @apiPermission None
 *
 * @apiParam {String} 	email 			Mandatory User's Email.
 * @apiParam {String} 	password 		Mandatory User's Password.
 *
 * @apiSuccess {Boolean} 	success		true/false.
 * @apiSuccess {String} 	message 	success/error message.
 */

router.post('/', function(req, res) {
	if (req.body.email)
		email = req.body.email

	if (req.body.password)
		passwd = crypto.createHash("sha256").update(req.body.password).digest('hex')

	User.findOne({email : email}, function(error, user){
		if (error){
			res.status(500).json({
				success : false,
				message : "Internal Server Error!"
			})
		}
		
		if (user){
			if (user.password==passwd){
			    res.json({
					success: true,
					message: "Successfully Logged In!"
				})
		    } else {
		    	// Unauthorized Access
		        res.status(401).json({
					success: false,
					message: "Authentication failed. Wrong email/password combination!"
				})
		    }

        } else {
            res.status(404).json({
				success: false,
				message: "User Doesn't Exists!!"
			})
        }
	})
})


module.exports = router